'use strict';

angular.module('mongolab', ['ngResource']).
    factory('MongoLabData', function($resource) {
        var MongoLabData = $resource('https://api.mongolab.com/api/1/databases/ttbt/collections/directions/:id',
            { apiKey: 'b_IZHryGbGMIW1QyiHRJKYmJi4yQ0yCN' },
            { update: { method: 'PUT' } }
        );


        var setTimestamp = function(direction) {
            var t = new Date();
            direction.lastUpdated = t.getTime();
        };


        MongoLabData.prototype.update = function(fn) {
            var id = this._id;
            setTimestamp(this);
            this._id = undefined;

            var self = this;
            return MongoLabData.update({id: id.$oid}, this, function() {
                self._id = id;
                fn();
            });
        };

        MongoLabData.prototype.save = function(direction, fn) {
            setTimestamp(this);
            return MongoLabData.save(direction, function() {
                fn();
            });
        };


        MongoLabData.prototype.destroy = function(fn) {
            return MongoLabData.remove({id: this._id.$oid}, fn);
        };

        return MongoLabData;
    });