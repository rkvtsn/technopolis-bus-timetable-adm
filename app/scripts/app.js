'use strict';

var app = angular .module('tbttAdminApp',
    [
        'ngRoute',
        'ngDialog',
        'ngTouch',
        'directionsController',
        'timetablesController',
        'timeTableServices',
        'mongolab'
    ]).config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.

            //directions
            when('/', {
                templateUrl: 'partials/direction/list.html',
                controller: 'DirectionListCtrl'
            }).
//            when('/directions/edit/:id', {
//                templateUrl: 'partials/direction/form.html',
//                controller: 'DirectionEditCtrl'
//            }).
//            when('/directions/add/', {
//                templateUrl: 'partials/direction/form.html',
//                controller: 'DirectionAddCtrl'
//            }).
//            when('/directions/delete/:id', {
//                templateUrl: 'partials/direction/delete.html',
//                controller: 'DirectionDeleteCtrl'
//            }).
//
//
//            //timettime
//            when('/time/edit/:id', {
//                templateUrl: 'partials/time/form.html',
//                controller: 'TimeEditCtrl'
//            }).
//            when('/time/add/:id', {
//                templateUrl: 'partials/time/form.html',
//                controller: 'TimeAddCtrl'
//            }).
//            when('/time/delete/:id', {
//                templateUrl: 'partials/time/delete.html',
//                controller: 'TimeDeleteCtrl'
//            }).

            //default
            otherwise({
                redirectTo: '/'
            });
}]);


app.factory('SplashScreen', function(){
    var self = this;
    self.visible = true;
    self.setVisible = function(x) { self.visible = x; };
    return this;
});

app.controller('SplashCtrl', ['$scope', 'SplashScreen', function($scope, SplashScreen) {
    $scope.Splash = SplashScreen;
}]);