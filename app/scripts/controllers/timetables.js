'use strict';

var timetablesController = angular.module('timetablesController', []);


directionsController.controller('TimetableCtrl', ['$scope','ngDialog',
    function ($scope, ngDialog) {

        $scope.showModalAddTime = function (dir) {
            $scope.dir = dir;
            ngDialog.open({
                template: 'partials/timetable/form.html',
                controller: 'TimetableAddCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

        $scope.showModalEditTime = function (dir, timetable) {
            $scope.timetable = timetable;
            $scope.dir = dir;
            ngDialog.open({
                template: 'partials/timetable/form.html',
                controller: 'TimetableEditCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

        $scope.showModalDeleteTime = function (dir, time) {
            $scope.time = time;
            $scope.dir = dir;
            ngDialog.open({
                template: 'partials/timetable/delete.html',
                controller: 'TimetableDeleteCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

    }]);

timetablesController.controller('TimetableAddCtrl', ['$scope','Timetable','ngDialog',
    function($scope, Timetable, ngDialog) {

        $scope.actionName = "Добавление";

        var timetable = Timetable.getTimetable();

        $scope.timeHoursOptions = timetable.hours;
        $scope.timeHoursSelected = $scope.timeHoursOptions[0];

        $scope.timeMinutesOptions = timetable.minutes;
        $scope.timeMinutesSelected = $scope.timeMinutesOptions[0];

        $scope.save = function() {
            Timetable.add($scope.dir, $scope.timeHoursSelected, $scope.timeMinutesSelected, function() {
                ngDialog.close();
            });
        };

        $scope.cancel = function () {
            ngDialog.close();
        };

    }]);


timetablesController.controller('TimetableEditCtrl', ['$scope','Timetable','ngDialog',
    function($scope, Timetable, ngDialog) {

        $scope.actionName = "Правка";
        $scope.isEdit = true;

        var timetable = Timetable.getTimetable();

        $scope.timeHoursOptions = timetable.hours;
        $scope.timeHoursSelected = $scope.timeHoursOptions[$scope.timetable.actual.hour];

        $scope.timeMinutesOptions = timetable.minutes;
        $scope.timeMinutesSelected = $scope.timeMinutesOptions[$scope.timetable.actual.minute];


        $scope.save = function() {
            Timetable.update($scope.dir, $scope.timetable.planned, $scope.timeHoursSelected, $scope.timeMinutesSelected, function() {
                ngDialog.close();
            });
        };

        $scope.cancel = function () {
            ngDialog.close();
        };

    }]);


timetablesController.controller('TimetableDeleteCtrl', ['$scope','Timetable','ngDialog',
    function($scope, Timetable, ngDialog) {

        $scope.cancel = function () {
            ngDialog.close();
        };

        $scope.save = function() {
            Timetable.destroy($scope.dir, $scope.time.planned, function() {
                ngDialog.close();
            });
        };

    }]);