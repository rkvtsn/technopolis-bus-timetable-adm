'use strict';

var directionsController = angular.module('directionsController', []);

directionsController.controller('DirectionListCtrl', ['$scope','MongoLabData','ngDialog','SplashScreen',
    function ($scope, Direction, ngDialog, SplashScreen) {

        $scope.directions = [];
        ($scope.refresh = function () {
            SplashScreen.setVisible(true);
            $scope.directions = Direction.query(function() {
                SplashScreen.setVisible(false);
            });
        })();

        $scope.showModalAddDirection = function () {
            ngDialog.open({
                template: 'partials/direction/form.html',
                controller: 'DirectionAddCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

        $scope.showModalEditDirection = function (id) {
            $scope.id = id;
            ngDialog.open({
                template: 'partials/direction/form.html',
                controller: 'DirectionEditCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

        $scope.showModalDeleteDirection = function (id) {
            $scope.id = id;
            ngDialog.open({
                template: 'partials/direction/delete.html',
                controller: 'DirectionDeleteCtrl',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };


        $scope.timeOffset = function (planned, actual) {
            var offset = 0;

            

            return offset;
        };

    }]);


directionsController.controller('DirectionAddCtrl', ['$scope','MongoLabData','ngDialog',
    function($scope, Direction, ngDialog) {
        $scope.actionName = "Добавить";

        $scope.direction = {
            title: "",
            id: "",
            timetable: []
        };

        $scope.cancel = function () {
            ngDialog.close();
        };

        $scope.save = function() {
            Direction.save($scope.direction, function() {
                $scope.refresh();
                ngDialog.close();
            });
        };

    }]);


directionsController.controller('DirectionEditCtrl', ['$scope','MongoLabData','ngDialog',
    function($scope, Direction, ngDialog) {

        $scope.actionName = "Изменить";

        var self = this;

        Direction.get({id: $scope.id}, function(direction) {
            self.original = direction;
            $scope.direction = new Direction(self.original);
        });

        $scope.cancel = function () {
            ngDialog.close();
        };

        $scope.save = function() {
            $scope.direction.update(function() {
                $scope.refresh();
                ngDialog.close();
            });
        };

    }]);

directionsController.controller('DirectionDeleteCtrl', ['$scope','MongoLabData','ngDialog',
    function($scope, Direction, ngDialog) {
        var self = this;

        Direction.get({id: $scope.id}, function(direction) {
            self.original = direction;
            $scope.direction = new Direction(self.original);
        });

        $scope.cancel = function () {
            ngDialog.close();
        };

        $scope.save = function() {
            self.original.destroy(function() {
                $scope.refresh();
                ngDialog.close();
            });
        };
    }]);



