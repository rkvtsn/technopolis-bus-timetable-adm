'use strict';

var timeTableServices = angular.module('timeTableServices', []);

timeTableServices.factory('Timetable', [
    function () {

        return {

            error: function (msg, result) {
                console.log('Error: ' + msg);
                return result;
            },

            add: function(direction, h, m, fn) {
                var time = this.setTime(h, m);

                if (this.find(direction, time) != -1) return this.error("Wrong time, not UNIQUE!", -1);

                direction.timetable.push({
                    planned: time,
                    actual: time
                });

                direction.update(fn);
            },

            destroy: function(direction, time, fn) {
                var index = this.find(direction, time);
                if (index == -1) return this.error("No such time in Timetable!", -1);

                direction.timetable.splice(index, 1);
                direction.update(fn);
            },

            update: function(direction, time, h, m, fn) {
                var index = this.find(direction, time);
                if (index == -1) return this.error("No such time in Timetable!", -1);

                direction.timetable[index].actual = this.setTime(h, m);
                direction.update(fn);
            },


            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////


            find: function(direction, time) {
                for (var i = 0; i < direction.timetable.length; i++) {
                    if (time.str == direction.timetable[i].planned.str) return i;
                }
                return -1;
            },

            setTime: function(h, m) {
                var time = { };
                time.hour = h.value;
                time.minute = m.value;
                time.str = h.label + ":" + m.label;
                return time;
            },

            __timeTable: "",

            getTimetable: function() {
                if (this.__timeTable != "") return this.__timeTable;

                var minutes=[];
                var hours = [];
                for (var i = 0; i < 60; i++) {
                    if (i <= 23)
                        hours.push({value: i, label: (i < 10) ? "0" + i : i + ""});
                    minutes.push({value: i, label: (i < 10) ? "0" + i : i + ""});
                }

                this.__timeTable = {
                    minutes: minutes,
                    hours: hours
                };
                return this.__timeTable;
            }

//            getById : function(directionId, timeId) {
//                var direction = Direction.getById(directionId);
//                for(var i = 0; i < direction.timetable.length; i++){
//                    if (direction.timetable[i].id == timeId) {
//                        var time = direction.timetable[i];
//                        time.index = i;
//                        var timeArr = time.time.split(':');
//                        time.h = timeArr[0];
//                        time.m = timeArr[1];
//                        return time;
//                    }
//                }
//            },
//
//            save : function(directionId, time) {
//                var direction = Direction.getById(directionId);
//                time.time = time.h + ":" + time.m;
//                direction.timetable[time.index] = angular.copy(time);
//            },
//
//            add : function(directionId, time) {
//                time.time = time.h + ":" + time.m;
//                var direction = Direction.getById(directionId);
//                time.id = this.getNewId(direction);
//                direction.timetable.push(time);
//            },
//
//            delete: function(directionId, timeId) {
//                var time = this.getById(directionId, timeId);
//                direction.timetable.splice(time.index, 1);
//            },
//
//            getNewId : function(direction) {
//                var len = direction.timetable.length;
//                if (len === 0) return 1;
//                return (Number(direction.timetable[len-1].id)) + 1;
//            }

        };

    }
]);















